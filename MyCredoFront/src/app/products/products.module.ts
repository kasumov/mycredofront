import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products.component';
import { CurrentSavingComponent } from './current-product/current-saving/current-saving.component';
import { CurrentAccountComponent } from './current-product/current-account/current-account.component';
import { CurrentLoanComponent } from './current-product/current-loan/current-loan.component';
import { ProductsMenuComponent } from './products-menu/products-menu.component';
import { AccountListComponent } from './product-lists/account-list/account-list.component';
import { SavingListComponent } from './product-lists/saving-list/saving-list.component';
import { CreditCardListComponent } from './product-lists/credit-card-list/credit-card-list.component';
import { LoanListComponent } from './product-lists/loan-list/loan-list.component';
import { CurrentCreditCardComponent } from './current-product/current-credit-card/current-credit-card.component';

@NgModule({
  declarations: [ProductsComponent, CurrentSavingComponent,
    CurrentAccountComponent, CurrentLoanComponent,
    ProductsMenuComponent, AccountListComponent,
    SavingListComponent, CreditCardListComponent, LoanListComponent, CurrentCreditCardComponent],
  imports: [
    CommonModule
  ]
})
export class ProductsModule { }
