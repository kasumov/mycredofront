import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { CoreModule } from './core/core.module';
import { LandingModule } from './landing/landing.module';
import { RedirectRoutingModule } from './redirect-routing.module';
import { ModalExampleComponent } from './modal-example/modal-example.component';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    ModalExampleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    LandingModule,
    SharedModule,
    RedirectRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
