import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { LandingComponent } from './landing.component';
import { ContactusComponent } from './contactus/contactus.component';

const routes: Routes = [
  { path: 'landing', redirectTo: 'landing/auth', pathMatch: 'full' },
  {
    path: 'landing', component: LandingComponent, children: [
      { path: 'auth', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'contact-us', component: ContactusComponent }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingRoutingModule { }
