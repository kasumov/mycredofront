import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OffersComponent } from './offers.component';
import { OffersMenuComponent } from './offers-menu/offers-menu.component';
import { OfferListComponent } from './offer-list/offer-list.component';
import { CurrentOfferComponent } from './current-offer/current-offer.component';

@NgModule({
  declarations: [OffersMenuComponent, OfferListComponent, OffersComponent, CurrentOfferComponent],
  imports: [
    CommonModule
  ]
})
export class OffersModule { }
