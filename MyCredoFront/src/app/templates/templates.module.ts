import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplatesComponent } from './templates.component';
import { EditMoneyTransferComponent } from './edit-money-transfer/edit-money-transfer.component';
import { EditAccountNumberComponent } from './edit-account-number/edit-account-number.component';
import { EditPhoneNumberComponent } from './edit-phone-number/edit-phone-number.component';
import { EditUtilityBillsComponent } from './edit-utility-bills/edit-utility-bills.component';
import { AddTemplateComponent } from './add-template/add-template.component';
import { AddAccountNumberTemplateComponent } from './add-template/add-account-number-template/add-account-number-template.component';
import { AddPhoneNumberTemplateComponent } from './add-template/add-phone-number-template/add-phone-number-template.component';
import { AddUtilityBillsTemplateComponent } from './add-template/add-utility-bills-template/add-utility-bills-template.component';
import { AddMoneyTransferTemplateComponent } from './add-template/add-money-transfer-template/add-money-transfer-template.component';

@NgModule({
  declarations: [TemplatesComponent, EditMoneyTransferComponent, EditAccountNumberComponent, EditPhoneNumberComponent, EditUtilityBillsComponent, AddTemplateComponent, AddAccountNumberTemplateComponent, AddPhoneNumberTemplateComponent, AddUtilityBillsTemplateComponent, AddMoneyTransferTemplateComponent],
  imports: [
    CommonModule
  ]
})
export class TemplatesModule { }
