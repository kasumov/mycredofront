import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransfersComponent } from './transfers.component';
import { MobileComponent } from './mobile/mobile.component';
import { TransferToOwnComponent } from './transfer-to-own/transfer-to-own.component';
import { TransferToSomeoneComponent } from './transfer-to-someone/transfer-to-someone.component';
import { RemittanceComponent } from './remittance/remittance.component';
import { UtilityComponent } from './utility/utility.component';
import { AddUtilityCategoriesComponent } from './utility/add-utility-categories/add-utility-categories.component';
import { AddUtilityProviderComponent } from './utility/add-utility-provider/add-utility-provider.component';

@NgModule({
  declarations: [TransfersComponent, MobileComponent,
    TransferToOwnComponent,
    TransferToSomeoneComponent, RemittanceComponent, UtilityComponent, AddUtilityCategoriesComponent, AddUtilityProviderComponent],
  imports: [
    CommonModule
  ]
})
export class TransfersModule { }
