import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TakeFastLoanComponent } from './take-fast-loan/take-fast-loan.component';
import { TakeCarLoanComponent } from './take-car-loan/take-car-loan.component';
import { TakeStudentLoanComponent } from './take-student-loan/take-student-loan.component';
import { TakeLoanDetailsComponent } from './take-loan-details/take-loan-details.component';

@NgModule({
  declarations: [TakeFastLoanComponent, TakeCarLoanComponent, TakeStudentLoanComponent, TakeLoanDetailsComponent],
  imports: [
    CommonModule
  ]
})
export class TakeALoanModule { }
