import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Account } from './products.model';

@Component({
  selector: 'app-account-select',
  templateUrl: './account-select.component.html',
  styleUrls: ['./account-select.component.scss']
})
export class AccountSelectComponent implements OnInit {
  selectedAccount?: any = {
    accountId: 1,
    accountNumber: 'GE12341253636236',
    currency: 'USD',
    availableBalance: 100000,
    blockedAmount: 40000,
    balance: 20000
  };
  accountsError?: any;
  accounts = [
    {
      accountId: 7,
      accountNumber: 'GE56CD0360000014996141',
      availableBalance: 0,
      availableBalanceEqu: 0,
      balance: 0,
      cards: [],
      category: 'მიმდინარე',
      currency: 'GEL',
      currencyPriority: 0,
    },
    {
      accountId: 6,
      accountNumber: 'GE56CD0360000014996141',
      availableBalance: 0,
      availableBalanceEqu: 0,
      balance: 0,
      cards: [],
      category: 'მიმდინარე',
      currency: 'GEL',
      currencyPriority: 0,
    },
    {
      accountId: 51,
      accountNumber: 'GE56CD0360000014996141',
      availableBalance: 0,
      availableBalanceEqu: 0,
      balance: 0,
      cards: [],
      category: 'მიმდინარე',
      currency: 'GEL',
      currencyPriority: 0,
    },
    {
      accountId: 3,
      accountNumber: 'GE56CD0360000014996141',
      availableBalance: 0,
      availableBalanceEqu: 0,
      balance: 0,
      cards: [],
      category: 'მიმდინარე',
      currency: 'GEL',
      currencyPriority: 0,
    }
  ];
  disabled = false;
  isLoading = false;
  clearable = true; // show all accounts option (transactions)

  select(account) {
    this.selectedAccount = account;
  }
  constructor() { }

  ngOnInit() {
  }

}
