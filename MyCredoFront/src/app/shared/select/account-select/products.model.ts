export class Account {
    accountId: number;
    accountNumber: string;
    currency: string;
    availableBalance: number;
    blockedAmount: number;
    balance: number;
}
