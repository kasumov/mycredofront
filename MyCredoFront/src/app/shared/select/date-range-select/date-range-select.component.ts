import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { DateRangeSelect } from './date-range-select.model';

@Component({
  selector: 'app-date-range-select',
  templateUrl: './date-range-select.component.html',
  styleUrls: ['./date-range-select.component.scss']
})
export class DateRangeSelectComponent implements OnInit {
  @Output() selectDateRangeInline = new EventEmitter<DateRangeSelect>();
  @Input() selectedDateRange: DateRangeSelect;
  @Input() disabled: boolean;

  options: Array<DateRangeSelect> = DateRangeSelect.options;

  showDatePickerModal = false;

  constructor() { }

  ngOnInit() {
  }

  
  openDatePickerModal() {
    this.showDatePickerModal = true;
  }

  closeDatePickerModal() {
    this.showDatePickerModal = false;
  }
  selectDates(event) {
    this.selectDateRangeInline.emit({
      type: 'inline',
      startDate: event.startDate,
      endDate: event.endDate,
      value: -2,
      label: `${event.startDate.getDate()}/${event.startDate.getMonth() + 1}/${event.startDate.getFullYear()}` + 
      ` - ${event.endDate.getDate()}/${event.endDate.getMonth() + 1}/${event.endDate.getFullYear()}`
    });

    this.closeDatePickerModal();
  }
  
  select(item: DateRangeSelect) {
    if (item.type === 'inline') {
      item.startDate = new Date();
      item.endDate = new Date();
      item.startDate.setMonth(item.startDate.getMonth() - item.value);
      this.selectDateRangeInline.emit(item);
    } else {
      this.openDatePickerModal();
    }
  }

}
