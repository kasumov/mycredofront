export class DateRangeSelect {
    static options: Array<DateRangeSelect> = [
        {
            type: 'inline',
            value: 3,
            label: '3 თვე'
        },
        {
            type: 'inline',
            value: 6,
            label: '6 თვე'
        },
        {
            type: 'inline',
            value: 12,
            label: '1 წელი'
        },
        {
            type: 'modal',
            value: -1,
            label: 'აირჩიეთ თარიღი'
        }
    ];
    type: string;
    value: number;
    label: string;
    startDate?: Date;
    endDate?: Date;
}
