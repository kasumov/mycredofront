export class SimpleSelect {
    static months: SimpleSelect[] = [
        {
            label: 'January',
            value: 'jan'
        }
    ];

    static currencies: SimpleSelect[] = [
        {
            label: 'Select Currency',
            value: '-1',
            // image: 'usd-link'
        },
        {
            label: 'USD',
            value: 'usd',
            //image: 'usd-link'
            image: '/assets/images/0/usd.png'
        },
        {
            label: 'GEL',
            value: 'gel',
            image: 'usd-link'
        },
        {
            label: 'RUB',
            value: 'rub',
            image: 'usd-link'
        }
    ];
    label: string;
    image?: string;
    value: any;
}
