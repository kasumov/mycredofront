import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { SimpleSelect } from './simple-select.model';

@Component({
  selector: 'app-simple-select',
  templateUrl: './simple-select.component.html',
  styleUrls: ['./simple-select.component.scss']
})
export class SimpleSelectComponent implements OnInit {
  options?: Array<SimpleSelect> = SimpleSelect.currencies;
  selectedItem?: SimpleSelect = SimpleSelect.currencies[0];
  // @Output() selectOption = new EventEmitter<SimpleSelect>();
  disabled = false;

  select(item: SimpleSelect) {
    this.selectedItem = item;
    // this.selectOption.emit(item);
  }

  constructor() { }

  ngOnInit() {
  }

}
