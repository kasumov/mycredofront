import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleSelectComponent } from './select/simple-select/simple-select.component';
import { AccountSelectComponent } from './select/account-select/account-select.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { ClickOutsideDirective } from './directive/click-outside/click-outside.directive';
import { ModalComponent } from './modal/modal.component';

@NgModule({
  declarations: [ClickOutsideDirective, DropdownComponent, AccountSelectComponent, SimpleSelectComponent, ModalComponent],
  imports: [
    CommonModule
  ],
  exports: [AccountSelectComponent, SimpleSelectComponent, ModalComponent]
})
export class SharedModule { }
