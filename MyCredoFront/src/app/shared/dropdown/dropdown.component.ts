import { Component, OnInit, HostListener, ElementRef, Input } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {
  @Input() disabled = false;
  selectState = 'closed';
  constructor() { }

  ngOnInit() {
  }

  close() {
    this.selectState = 'closed';
  }

  toggleState() {
    if (this.disabled) { return; }
    if (this.selectState === 'open') {
      this.selectState = 'closed';
    } else {
      this.selectState = 'open';
    }
  }

}
