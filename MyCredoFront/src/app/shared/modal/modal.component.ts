import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  // @Input() AllowClose: boolean = false;
  @Output() closeModal = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  closeClick(event) {
    if (event.target.classList.contains('modalContainer') || event.target.classList.contains('closeButton')) {
      this.closeModal.emit();
    }
  }

}
