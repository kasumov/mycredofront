import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthGuard } from './service/auth-guard.service';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';

import { CoreComponent } from './core.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MenuComponent } from './menu/menu.component';
import { CoreRoutingModule } from './core-routing.module';
import { HomeModule } from '../home/home.module';
import { TutorialFooterComponent } from './tutorial-footer/tutorial-footer.component';
import { TransfersModule } from '../transfers/transfers.module';
import { TemplatesModule } from '../templates/templates.module';
import { FaqModule } from '../faq/faq.module';
import { MyFinancesModule } from '../my-finances/my-finances.module';
import { OffersModule } from '../offers/offers.module';
import { ProductsModule } from '../products/products.module';
import { SettingsModule } from '../settings/settings.module';
import { MessagesModule } from '../messages/messages.module';
import { CalendarModule } from '../calendar/calendar.module';
import { CurrencyConverterModule } from '../currency-converter/currency-converter.module';
import { TakeALoanModule } from '../take-a-loan/take-a-loan.module';
import { TransactionsModule } from '../transactions/transactions.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [CoreComponent, HeaderComponent, FooterComponent, MenuComponent, TutorialFooterComponent],
  imports: [
    CommonModule,
    CoreRoutingModule,
    HttpClientModule,
    GraphQLModule,
    SharedModule,
    HomeModule,
    TransfersModule,
    TemplatesModule,
    FaqModule,
    MyFinancesModule,
    OffersModule,
    ProductsModule,
    SettingsModule,
    MessagesModule,
    CalendarModule,
    CurrencyConverterModule,
    TakeALoanModule,
    TransactionsModule
  ],
  exports: [CoreComponent],
  providers: [AuthGuard]
})
export class CoreModule { }
