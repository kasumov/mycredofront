import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { HomeComponent } from '../home/home.component';
import { CoreComponent } from './core.component';
import { TransfersComponent } from '../transfers/transfers.component';
import { MobileComponent } from '../transfers/mobile/mobile.component';
import { TransferToOwnComponent } from '../transfers/transfer-to-own/transfer-to-own.component';
import { TransferToSomeoneComponent } from '../transfers/transfer-to-someone/transfer-to-someone.component';
import { RemittanceComponent } from '../transfers/remittance/remittance.component';
import { UtilityComponent } from '../transfers/utility/utility.component';
import { AddUtilityCategoriesComponent } from '../transfers/utility/add-utility-categories/add-utility-categories.component';
import { AddUtilityProviderComponent } from '../transfers/utility/add-utility-provider/add-utility-provider.component';
import { TemplatesComponent } from '../templates/templates.component';
import { FaqComponent } from '../faq/faq.component';
import { MyFinancesComponent } from '../my-finances/my-finances.component';
import { OffersComponent } from '../offers/offers.component';
import { CurrentOfferComponent } from '../offers/current-offer/current-offer.component';
import { ProductsComponent } from '../products/products.component';
import { CurrentLoanComponent } from '../products/current-product/current-loan/current-loan.component';
import { CurrentAccountComponent } from '../products/current-product/current-account/current-account.component';
import { CurrentSavingComponent } from '../products/current-product/current-saving/current-saving.component';
import { CurrentCreditCardComponent } from '../products/current-product/current-credit-card/current-credit-card.component';
import { AddTemplateComponent } from '../templates/add-template/add-template.component';
import { SettingsComponent } from '../settings/settings.component';
import { MessagesComponent } from '../messages/messages.component';
import { CalendarComponent } from '../calendar/calendar.component';
import { AddEventComponent } from '../calendar/add-event/add-event.component';
import { EditEventComponent } from '../calendar/edit-event/edit-event.component';
import { CurrencyConverterComponent } from '../currency-converter/currency-converter.component';
import { TakeFastLoanComponent } from '../take-a-loan/take-fast-loan/take-fast-loan.component';
import { TakeCarLoanComponent } from '../take-a-loan/take-car-loan/take-car-loan.component';
import { TakeStudentLoanComponent } from '../take-a-loan/take-student-loan/take-student-loan.component';
import { TakeLoanDetailsComponent } from '../take-a-loan/take-loan-details/take-loan-details.component';
import { TransactionsComponent } from '../transactions/transactions.component';
import { ModalExampleComponent } from '../modal-example/modal-example.component';

const routes: Routes = [
    {
        path: '', component: CoreComponent, children: [
            { path: '', component: HomeComponent },
            /* Transfers Block */
            { path: 'payment/mobile', component: MobileComponent },
            { path: 'payment/utility', component: UtilityComponent },
            { path: 'payment/utility/categories', component: AddUtilityCategoriesComponent },
            { path: 'payment/utility/provider/:id', component: AddUtilityProviderComponent },
            { path: 'transfer/to-own-account', component: TransferToOwnComponent },
            { path: 'transfer/to-someone-account', component: TransferToSomeoneComponent },
            { path: 'remittance', component: RemittanceComponent },
            { path: 'transfers', component: TransfersComponent },
            /* Faq */
            { path: 'faq', component: FaqComponent },
            /* My finances */
            { path: 'my-finances', component: MyFinancesComponent },
            /* Offers */
            { path: 'offers', component: OffersComponent },
            { path: 'offer/:id', component: CurrentOfferComponent },
            /* Products */
            { path: 'product/:type', component: ProductsComponent }, // products type enter like products/loans (accounts, )
            { path: 'product/loan/:id', component: CurrentLoanComponent },
            { path: 'product/credit-card/:id', component: CurrentCreditCardComponent },
            { path: 'product/account/:id', component: CurrentAccountComponent },
            { path: 'product/saving/:id', component: CurrentSavingComponent },
            /* Templates Block */
            { path: 'templates/add', component: AddTemplateComponent },
            { path: 'templates', component: TemplatesComponent },
            /* Settings Block */
            { path: 'settings', component: SettingsComponent },
            /* Messages Block */
            { path: 'messages', component: MessagesComponent },
            /* Calendar Block */
            { path: 'calendar', component: CalendarComponent },
            { path: 'calendar/add-event', component: AddEventComponent },
            { path: 'calendar/edit-event', component: EditEventComponent },
            /* Currency converter Block */
            { path: 'currency-converter', component: CurrencyConverterComponent },
            /* Take a loan */
            { path: 'take-a-loan/fast', component: TakeFastLoanComponent },
            { path: 'take-a-loan/car', component: TakeCarLoanComponent },
            { path: 'take-a-loan/student', component: TakeStudentLoanComponent },
            { path: 'take-a-loan/:type/details', component: TakeLoanDetailsComponent },
            /* Transactions */
            { path: 'transactions', component: TransactionsComponent },
            { path: 'example-modals', component: ModalExampleComponent }
        ],
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CoreRoutingModule { }
