import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { ProductsComponent } from './products/products.component';
import { TransactionListComponent } from './transaction-list/transaction-list.component';
import { AdsComponent } from './ads/ads.component';
import { TransferBoxComponent } from './transfer-box/transfer-box.component';
import { AdsRightComponent } from './ads-right/ads-right.component';
import { ExchangeBoxComponent } from './exchange-box/exchange-box.component';
import { CalendarBoxComponent } from './calendar-box/calendar-box.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [HomeComponent, ProductsComponent,
    TransactionListComponent, AdsComponent, TransferBoxComponent,
    AdsRightComponent, ExchangeBoxComponent, CalendarBoxComponent],
  imports: [
    CommonModule,
    SharedModule,

  ]
})
export class HomeModule { }
