import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyFinancesComponent } from './my-finances.component';

@NgModule({
  declarations: [MyFinancesComponent],
  imports: [
    CommonModule
  ]
})
export class MyFinancesModule { }
