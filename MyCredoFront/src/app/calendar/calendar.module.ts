import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarComponent } from './calendar.component';
import { AddEventComponent } from './add-event/add-event.component';
import { EditEventComponent } from './edit-event/edit-event.component';

@NgModule({
  declarations: [CalendarComponent, AddEventComponent, EditEventComponent],
  imports: [
    CommonModule
  ]
})
export class CalendarModule { }
